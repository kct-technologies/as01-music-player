const redisConfig = {
    REDIS_PORT: process.env.AGMT_REDIS_PORT || 6379,
    REDIS_HOST: process.env.AGMT_REDIS_HOST || '127.0.0.1',
}
module.exports = redisConfig;