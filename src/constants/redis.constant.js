const redisKeys = {
    USERS: "AGMT_USERS",
    SONGS: "AGMT_SONGS",
    ALL_SOCKETS: "AGMT_ALL_SKT",

    // dynamic keys
    USER_SOCKETS: (userId) => `AGMT_U_SKTS_${userId}`,
    USER_ON_SOCKET: (socketId) => `AGMT_U_ON_SNG_${socketId}`,
    USER_CURRENT_SONG: (userId) => `AGMT_U_CRNT_SNG_${userId}`,
}
exports.redisKeys = redisKeys;