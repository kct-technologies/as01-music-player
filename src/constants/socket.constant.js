const listeningEvents = {
    CONNECT: `AGMT_CONNECT`,
    SONG_PLAY: `AGMT_SONG_PLAYING`,
}
exports.listeningEvents = listeningEvents;

const emittingEvents = {
    SONG_PLAY: `AGMT_SONG_PLAYING`,
}
exports.emittingEvents = emittingEvents;
