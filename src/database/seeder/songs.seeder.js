const songRepository = require("../../repositories/song.repository");

const runSongSeeder = async () => {
    let dataToInsert = [];
    for (let i = 1; i <= 5; i++) {
        dataToInsert.push({
            id: i,
            name: `Song_${i}`,
            duration: Math.floor(Math.random() * 200) + 300,
            artist: `artist_${i}`,
            album: `album_${i}`,
        })
    }
    dataToInsert.forEach((song) => {
        songRepository.insert(song);
    })
}

module.exports = runSongSeeder;