const userRepository = require("../../repositories/user.repository");

const runUserSeeder = async () => {
    let dataToInsert = [
        {
            id: 1,
            username: "user1",
            password: "password1",
        },
        {
            id: 2,
            username: "user2",
            password: "password2",
        },
    ];
    dataToInsert.forEach((user) => {
        userRepository.insert(user);
    })
}

module.exports = runUserSeeder;