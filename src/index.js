const env = require('dotenv');
env.config({path: __dirname + '/../.env'});
const http = require("http");
const express = require("express");
const socket = require("socket.io");
const socketService = require("./sockets/socket.service");
const redisService = require("./services/redis.service");
const userRoutes = require("./routes/user.route");

// app related constants
const app = express();
const server = http.createServer(app);

// socket related constants
const io = socket(server);

app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(userRoutes);

socketService.initSocket(io);
redisService.initRedis();

// Server related constants
const PORT = process.env.AGMT_PORT;

// Starting the server here
server.listen(PORT, () => {
    console.log(`Server started on ${server.address().port}`);
})