const redisService = require("../services/redis.service");
const redisConstant = require("../constants/redis.constant");
const userRepository = require("./user.repository");

const addUserSocket = async (userId, socketId) => {
    const redisClient = redisService.getRedisClient();
    await redisClient.LPUSH(redisConstant.redisKeys.USER_SOCKETS(userId), socketId);
    await redisClient.SET(redisConstant.redisKeys.USER_ON_SOCKET(socketId), userId);
    await redisClient.LPUSH(redisConstant.redisKeys.ALL_SOCKETS, socketId);
}
exports.addUserSocket = addUserSocket;

const getUserBySocketId = async (socketId) => {
    const redisClient = redisService.getRedisClient();
    const userId = await redisClient.GET(redisConstant.redisKeys.USER_ON_SOCKET(socketId));
    return await userRepository.findById(userId);
}
exports.getUserBySocketId = getUserBySocketId;

const updateUserCurrentSong = async (user, songId) => {
    const redisClient = redisService.getRedisClient();
    return await redisClient.SET(redisConstant.redisKeys.USER_CURRENT_SONG(user.id), songId);
}
exports.updateUserCurrentSong = updateUserCurrentSong;

const getSocketsOfUser = async (userId) => {
    const redisClient = redisService.getRedisClient();
    return await redisClient.LRANGE(redisConstant.redisKeys.USER_SOCKETS(userId), 0, -1);
}
exports.getSocketsOfUser = getSocketsOfUser;

const getUserCurrentSong = async(userId) => {
    const redisClient = redisService.getRedisClient();
    return await redisClient.GET(redisConstant.redisKeys.USER_CURRENT_SONG(userId));
}
exports.getUserCurrentSong = getUserCurrentSong;