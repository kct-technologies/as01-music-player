const redisService = require("../services/redis.service");
const redisConstant = require("../constants/redis.constant");

const findAll = async () => {
    const redisClient = redisService.getRedisClient();
    const songs = await redisClient.LRANGE(redisConstant.redisKeys.SONGS, 0, -1);
    songs?.forEach((jsonSong, i) => {
        songs[i] = JSON.parse(jsonSong);
    });
    return songs;
}
exports.findAll = findAll;

const insert = async (song) => {
    const jsonSong = JSON.stringify(song);
    const redisClient = redisService.getRedisClient();
    const songs = await findAll();
    let insertFlag = true;
    for (const dbSong of songs) {
        if (dbSong.id === song.id) {
            console.log("song already exists");
            insertFlag = false;
            break;
        }
    }
    if (insertFlag) {
        redisClient.LPUSH(redisConstant.redisKeys.SONGS, jsonSong);
    }
}
exports.insert = insert;