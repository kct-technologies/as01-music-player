const redisService = require("../services/redis.service");
const redisConstant = require("../constants/redis.constant");

const findAll = async () => {
    const redisClient = redisService.getRedisClient();
    const users = await redisClient.LRANGE(redisConstant.redisKeys.USERS, 0, -1);
    users?.forEach((jsonUser, i) => {
        users[i] = JSON.parse(jsonUser);
    });
    return users;
}
exports.findAll = findAll;

const findById = async (userId) => {
    const users = await findAll();
    for (const user of users) {
        if (user.id == userId) {
            return user;
        }
    }
    return null;
}
exports.findById = findById;


const insert = async (user) => {
    const jsonUser = JSON.stringify(user);
    const redisClient = redisService.getRedisClient();
    const users = await findAll();
    let insertFlag = true;
    for (const dbUser of users) {
        if (dbUser.id === user.id) {
            console.log("user already exists");
            insertFlag = false;
            break;
        }
    }
    if (insertFlag) {
        redisClient.LPUSH(redisConstant.redisKeys.USERS, jsonUser);
    }
}
exports.insert = insert;