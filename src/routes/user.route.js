const express = require('express');
const router = express.Router();
const userRepository = require("../repositories/user.repository");
const songRepository = require("../repositories/song.repository");
const socketRepository = require("../repositories/socket.repository");

// Dummy api to test the application
router.get("/tests", (req, res) => {
    return res.json({
        success: true,
        time: new Date(),
    });
})

router.post("/login", async (req, res) => {
    const users = await userRepository.findAll();
    let flag = false;
    let data = null;
    for (const user of users) {
        if (user.username === req.body.username && user.password === req.body.password) {
            flag = true;
            data = user;
        }
    }

    return res.json({
        success: flag,
        user: data
    })
});

router.get("/songs", async (req, res) => {
    const songs = await songRepository.findAll();
    return res.json({success: true, songs});
})

router.post('/user/current-song', async (req, res) => {
    if (!req.body.userId) {
        return res.json({success: false, msg: "Song id required"});
    }
    const song = await socketRepository.getUserCurrentSong(req.body.songId);
    return res.json({
        success: true,
        song
    })
})

module.exports = router;