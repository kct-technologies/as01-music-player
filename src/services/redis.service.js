const redis = require("redis");
const {promisify} = require("util")
const redisConstant = require("../config/redis.config");

const userSeeder = require("../database/seeder/user.seeder");
const songSeeder = require("../database/seeder/songs.seeder");


let client = null;
let redisClient = null;
const initRedis = () => {
    client = redis.createClient(redisConstant.REDIS_PORT, redisConstant.REDIS_HOST);
    client.on("connect", () => {
        console.log("connected to redis client");
    });
    initRedisClient();
    runSeeder().then(() => console.log("Seeded data")).catch((err) => console.log("Error in Seed", err));
}
exports.initRedis = initRedis;

const initRedisClient = () => {
    redisClient = {
        SET: promisify(client.SET).bind(client),
        LPUSH: promisify(client.LPUSH).bind(client),
        GET: promisify(client.GET).bind(client),
        LRANGE: promisify(client.LRANGE).bind(client),
        LREM: promisify(client.LREM).bind(client),
        DEL: promisify(client.DEL).bind(client),
        EXPIRE: promisify(client.EXPIRE).bind(client),
    }
}

const getRedisClient = () => {
    return redisClient;
}
exports.getRedisClient = getRedisClient;

const runSeeder = async () => {
    await userSeeder(redisClient);
    await songSeeder();
}
