const socketRepository = require("../repositories/socket.repository");

const emitToUserSockets = (io, eventName, userId, dataToSend) => {
    const sockets = socketRepository.getSocketsOfUser(userId);
    sockets.forEach((socketId) => {
        io.to(socketId).emit(eventName, dataToSend);
    })
}
exports.emitToUserSockets = emitToUserSockets;