const socketConstant = require("../constants/socket.constant");
const userSocket = require("./user.socket");

const initSocket = (io) => {
    io.on("connection", function (socket) {
        socket.on(socketConstant.listeningEvents.CONNECT, (data) => userSocket.userConnected(io, socket, data));
        socket.on(socketConstant.listeningEvents.SONG_PLAY, (data) => userSocket.userStartedSong(io, socket, data));
    });
}
exports.initSocket = initSocket;