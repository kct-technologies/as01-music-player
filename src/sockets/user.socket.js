const socketConstant = require("../constants/socket.constant");
const socketRepository = require("../repositories/socket.repository");
const emitter = require("./emiter.socket");

/**
 * @param io
 * @param socket
 * @param {{user: {id: int}}} data
 * @returns {Promise<void>}
 */
const userConnected = async (io, socket, data) => {
    if (!data?.user?.id) {
        console.log("ERROR IN USER CONNECT", data);
        return;
    }
    await socketRepository.addUserSocket(data.user.id, socket.id);

}
exports.userConnected = userConnected;

/**
 * @param io
 * @param socket
 * @param {{current_song: id}}data
 * @returns {Promise<void>}
 */
const userStartedSong = async (io, socket, data) => {
    if (!data?.current_song) {
        console.log("ERROR IN PLAYING USER SONG CONNECT", data);
        return;
    }
    const user = await socketRepository.getUserBySocketId(socket.id);
    if (user) {
        await socketRepository.updateUserCurrentSong(user, data.current_song);
    } else {
        console.log("no user found for the song update", {data, user});
    }
    emitter.emitToUserSockets(io, socketConstant.emittingEvents.SONG_PLAY, user.id, data);
}

exports.userStartedSong = userStartedSong;
